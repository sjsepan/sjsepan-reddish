# Greenish Color Theme - Change Log

## [0.3.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.3]

- update readme and screenshot

## [0.3.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- focusborder to match theme
- define window border in custom mode
- fix menu FG/BG

## [0.3.1]

- fix manifest and pub WF

## [0.3.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.2.3 for those who prefer the earlier style
- match statusbar FG/BG

## [0.2.3]

- make lst act sel BG transparent

## [0.2.2]

- fix syntax FG contrast on exceptional cases:
illegal
Broken
Deprecated
Unimplemented

## [0.2.1]

- fix scrollbar, minimap slider transparencies:
light:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",

## [0.2.0]

- fix contrast
- retain v0.1.3 for those who prefer prior scheme
- reverse activitybar, line-number FG/BG
- darken FG text
- fix panel title BG
- fix terminal BG
- fix breadcrumb background
- fix tab highlighting
- make BG more consistent
- fix terminal border
- dim borders
- lighten input BG
- lighten widget BG
- fix input border
- fix badges
- fix sidebar section header BG
- fix widget shadow
- fix readme MD

## [0.1.3]

- fix contrast of notification, hover widget FG/BG

## [0.1.2]

- hover widget FG/BG

## [0.1.1]

- notification FG/BG

## [0.1.0]

- tab borders
- scrollbar and widget shadows
- lighten background of editor and general panels, for contrast
- fix color match in activity bar, list, commands, find
- use syntax colors from humane-like
- keep old version of .VSIX for those who like original colors

## [0.0.3]

- scale icon down
- chg icon colorspace

## [0.0.2]

- rename pub and add manifest values

## [0.0.1]

- Initial release
