# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./sjsepan-reddish-0.3.4.vsix
npx ovsx publish sjsepan-reddish-0.3.4.vsix --debug --pat <PAT>