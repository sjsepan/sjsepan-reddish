# Reddish Theme

Reddish light color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-reddish_code.png](./images/sjsepan-reddish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-reddish_codium.png](./images/sjsepan-reddish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-reddish_codedev.png](./images/sjsepan-reddish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-reddish_ads.png](./images/sjsepan-reddish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-reddish_theia.png](./images/sjsepan-reddish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-reddish_positron.png](./images/sjsepan-reddish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/10/2025
